#!/usr/bin/env python3

# ------------------------------------------------------------------------------
# PROBLEMA
# listar los archivos del directorio descargas del usuario actual
# primero imprimir el nombre del usuario y luego en cada linea siguiente los archivos
# ------------------------------------------------------------------------------

import os

print("------------------------------------")
dir = os.path.expanduser("~")
dir = os.path.join(dir, "Downloads")
print("Directorio: ", dir)
print("------------------------------------")
for file in os.listdir(dir):
        if os.path.isfile(os.path.join(dir, file)):
            print(file)